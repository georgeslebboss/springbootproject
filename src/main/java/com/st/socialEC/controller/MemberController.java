 
package com.st.socialEC.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import com.st.socialEC.exception.ResourceNotFoundException;
import com.st.socialEC.model.Member;
import com.st.socialEC.repository.MemberRepository;

 
@RestController
@RequestMapping("/")
public class MemberController {

	@Autowired
	private MemberRepository memberRepository;
	
    @GetMapping("/members")
    public List<Member> getAllMembers() {
        return memberRepository.findAll();
    }
	
    @GetMapping("/members/{id}")
    public ResponseEntity<Member> getMemberById(@PathVariable(value = "id") Long memberId)
        throws com.st.socialEC.exception.ResourceNotFoundException {
        Member member = memberRepository.findById(memberId)
          .orElseThrow(() -> new ResourceNotFoundException("Member not found for this id :: " + memberId));
        return ResponseEntity.ok().body(member);
    }
    
    @PostMapping("/members")
    public Member createMember(@Valid @RequestBody Member member) {
        return memberRepository.save(member);
    }
    
    @PutMapping("/members/{id}")
    public ResponseEntity<Member> updateMember(@PathVariable(value = "id") Long memberId,
         @Valid @RequestBody Member memberDetails) throws ResourceNotFoundException {
        Member member = memberRepository.findById(memberId)
        		.orElseThrow(() -> new ResourceNotFoundException("Member not found for this id :: " + memberId));

        member.setActive(memberDetails.getActive());
        member.setAddress1(memberDetails.getAddress1());
        member.setAddress2(memberDetails.getAddress2());
        member.setAddress3(memberDetails.getAddress3());

        member.setBrandLogo(memberDetails.getBrandLogo());
        member.setCity(memberDetails.getCity());
        member.setCountry(memberDetails.getCountry());
        member.setCountryZipcode(memberDetails.getCountryZipcode());
        member.setDevice(memberDetails.getDevice());
        member.setEmail(memberDetails.getEmail());
        member.setGender(memberDetails.getGender());
        member.setIsguest(memberDetails.getIsguest());
        member.setPhoneNumber(memberDetails.getPhoneNumber());
        member.setProfilePic(memberDetails.getProfilePic());
        member.setSignupWith(memberDetails.getSignupWith());
        member.setState(memberDetails.getState());
        member.setTradeName(memberDetails.getTradeName());
        
        
        member.setFirstName(memberDetails.getFirstName());
        member.setLastName(memberDetails.getLastName());
        
        
        
        
        member.setDateModified(memberDetails.getDateModified());
        member.setUserModified(memberDetails.getUserModified());

        
        
        final Member updatedMember = memberRepository.save(member);
        return ResponseEntity.ok(updatedMember);
    }

    @DeleteMapping("/members/{id}")
    public Map<String, Boolean> deleteMember(@PathVariable(value = "id") Long memberId)
         throws ResourceNotFoundException {
        Member member = memberRepository.findById(memberId)
       .orElseThrow(() -> new ResourceNotFoundException("Member not found for this id :: " + memberId));

        memberRepository.delete(member);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
