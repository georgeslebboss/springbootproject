package com.st.socialEC.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import com.st.socialEC.exception.ResourceNotFoundException;
import com.st.socialEC.model.Gender;
import com.st.socialEC.repository.GenderRepository;

 
@RestController
@RequestMapping("/")
public class GenderController {

	@Autowired
	private GenderRepository genderRepository;
	
    @GetMapping("/genders")
    public List<Gender> getAllGenders() {
        return genderRepository.findAll();
    }
	
    @GetMapping("/genders/{id}")
    public ResponseEntity<Gender> getGenderById(@PathVariable(value = "id") Long genderId)
        throws com.st.socialEC.exception.ResourceNotFoundException {
        Gender gender = genderRepository.findById(genderId)
          .orElseThrow(() -> new ResourceNotFoundException("Gender not found for this id :: " + genderId));
        return ResponseEntity.ok().body(gender);
    }
    
    @PostMapping("/genders")
    public Gender createGender(@Valid @RequestBody Gender gender) {
        return genderRepository.save(gender);
    }
    
    @PutMapping("/genders/{id}")
    public ResponseEntity<Gender> updateGender(@PathVariable(value = "id") Long genderId,
         @Valid @RequestBody Gender genderDetails) throws ResourceNotFoundException {
        Gender gender = genderRepository.findById(genderId)
        		.orElseThrow(() -> new ResourceNotFoundException("Gender not found for this id :: " + genderId));

        gender.setActive(genderDetails.getActive());
        gender.setGenderDesc(genderDetails.getGenderDesc());
        gender.setDateModified(genderDetails.getDateModified());
        gender.setUserModified(genderDetails.getUserModified()); 
        
        final Gender updatedGender = genderRepository.save(gender);
        return ResponseEntity.ok(updatedGender);
    }

    @DeleteMapping("/genders/{id}")
    public Map<String, Boolean> deleteGender(@PathVariable(value = "id") Long genderId)
         throws ResourceNotFoundException {
        Gender gender = genderRepository.findById(genderId)
       .orElseThrow(() -> new ResourceNotFoundException("Gender not found for this id :: " + genderId));

        genderRepository.delete(gender);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
