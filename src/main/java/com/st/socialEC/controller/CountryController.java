package com.st.socialEC.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import com.st.socialEC.exception.ResourceNotFoundException;
import com.st.socialEC.model.Country;
import com.st.socialEC.repository.CountryRepository;

 
@RestController
@RequestMapping("/")
public class CountryController {

	@Autowired
	private CountryRepository countryRepository;
	
    @GetMapping("/countries")
    public List<Country> getAllCountrys() {
        return countryRepository.findAll();
    }
	
    @GetMapping("/countries/{id}")
    public ResponseEntity<Country> getCountryById(@PathVariable(value = "id") Long countryId)
        throws com.st.socialEC.exception.ResourceNotFoundException {
        Country country = countryRepository.findById(countryId)
          .orElseThrow(() -> new ResourceNotFoundException("Country not found for this id :: " + countryId));
        return ResponseEntity.ok().body(country);
    }
    
    @PostMapping("/countries")
    public Country createCountry(@Valid @RequestBody Country country) {
        return countryRepository.save(country);
    }
    
    @PutMapping("/countries/{id}")
    public ResponseEntity<Country> updateCountry(@PathVariable(value = "id") Long countryId,
         @Valid @RequestBody Country countryDetails) throws ResourceNotFoundException {
        Country country = countryRepository.findById(countryId)
        		.orElseThrow(() -> new ResourceNotFoundException("Country not found for this id :: " + countryId));

        country.setActive(countryDetails.getActive());
        country.setCountryName(countryDetails.getCountryName());
        country.setDateModified(countryDetails.getDateModified());
        country.setUserModified(countryDetails.getUserModified());
        country.setContinent(countryDetails.getContinent());
        
        
        final Country updatedCountry = countryRepository.save(country);
        return ResponseEntity.ok(updatedCountry);
    }

    @DeleteMapping("/countrys/{id}")
    public Map<String, Boolean> deleteCountry(@PathVariable(value = "id") Long countryId)
         throws ResourceNotFoundException {
        Country country = countryRepository.findById(countryId)
       .orElseThrow(() -> new ResourceNotFoundException("Country not found for this id :: " + countryId));

        countryRepository.delete(country);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
