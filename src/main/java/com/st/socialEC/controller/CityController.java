 
package com.st.socialEC.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import com.st.socialEC.exception.ResourceNotFoundException;
import com.st.socialEC.model.City;
import com.st.socialEC.repository.CityRepository;

 
@RestController
@RequestMapping("/")
public class CityController {

	@Autowired
	private CityRepository cityRepository;
	
    @GetMapping("/cities")
    public List<City> getAllCitys() {
        return cityRepository.findAll();
    }
	
    @GetMapping("/cities/{id}")
    public ResponseEntity<City> getCityById(@PathVariable(value = "id") Long cityId)
        throws com.st.socialEC.exception.ResourceNotFoundException {
        City city = cityRepository.findById(cityId)
          .orElseThrow(() -> new ResourceNotFoundException("City not found for this id :: " + cityId));
        return ResponseEntity.ok().body(city);
    }
    
    @PostMapping("/citys")
    public City createCity(@Valid @RequestBody City city) {
        return cityRepository.save(city);
    }
    
    @PutMapping("/citys/{id}")
    public ResponseEntity<City> updateCity(@PathVariable(value = "id") Long cityId,
         @Valid @RequestBody City cityDetails) throws ResourceNotFoundException {
        City city = cityRepository.findById(cityId)
        		.orElseThrow(() -> new ResourceNotFoundException("City not found for this id :: " + cityId));

        city.setActive(cityDetails.getActive());
        city.setCityName(cityDetails.getCityName());
        city.setDateModified(cityDetails.getDateModified());
        city.setUserModified(cityDetails.getUserModified());
        city.setCoordinate(cityDetails.getCoordinate());
        
        
        final City updatedCity = cityRepository.save(city);
        return ResponseEntity.ok(updatedCity);
    }

    @DeleteMapping("/citys/{id}")
    public Map<String, Boolean> deleteCity(@PathVariable(value = "id") Long cityId)
         throws ResourceNotFoundException {
        City city = cityRepository.findById(cityId)
       .orElseThrow(() -> new ResourceNotFoundException("City not found for this id :: " + cityId));

        cityRepository.delete(city);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
