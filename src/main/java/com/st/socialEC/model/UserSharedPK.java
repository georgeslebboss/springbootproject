package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_shared database table.
 * 
 */
@Embeddable
public class UserSharedPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="member_user_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer memberUserId;

	@Column(name="item_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer itemId;

	public UserSharedPK() {
	}
	public Integer getMemberUserId() {
		return this.memberUserId;
	}
	public void setMemberUserId(Integer memberUserId) {
		this.memberUserId = memberUserId;
	}
	public Integer getItemId() {
		return this.itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserSharedPK)) {
			return false;
		}
		UserSharedPK castOther = (UserSharedPK)other;
		return 
			this.memberUserId.equals(castOther.memberUserId)
			&& this.itemId.equals(castOther.itemId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.memberUserId.hashCode();
		hash = hash * prime + this.itemId.hashCode();
		
		return hash;
	}
}