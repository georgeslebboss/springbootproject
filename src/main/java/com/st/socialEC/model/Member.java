package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the members database table.
 * 
 */
@Entity
@Table(name="members")
@NamedQuery(name="Member.findAll", query="SELECT m FROM Member m")
public class Member implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="member_id", unique=true, nullable=false)
	private Long memberId;

	private Boolean active;

	@Column(length=255)
	private String address1;

	@Column(length=255)
	private String address2;

	@Column(length=255)
	private String address3;

	@Column(length=255)
	private String address4;

	@Column(name="brand_logo", length=255)
	private String brandLogo;

	@Column(name="country_zipcode", length=10)
	private String countryZipcode;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(length=255)
	private String device;

	@Column(length=256)
	private String email;

	@Column(name="first_name", nullable=false, length=255)
	private String firstName;

	private Boolean isguest;

	@Column(name="last_name", nullable=false, length=255)
	private String lastName;

	@Column(name="phone_number", length=255)
	private String phoneNumber;

	@Column(name="profile_pic", length=255)
	private String profilePic;

	@Column(name="signup_with", length=255)
	private String signupWith;

	@Column(name="trade_name", length=255)
	private String tradeName;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	@Column(name="user_name", nullable=false, length=255)
	private String userName;

	@Column(name="user_password", nullable=false, length=255)
	private String userPassword;

	//bi-directional many-to-one association to Basket
	@OneToMany(mappedBy="member")
	private List<Basket> baskets;

	//bi-directional many-to-one association to Basta
	@OneToMany(mappedBy="member")
	private List<Basta> bastas;

	//bi-directional many-to-one association to CallDone
	@OneToMany(mappedBy="member")
	private List<CallDone> callDones;

	//bi-directional many-to-one association to Follower
	@OneToMany(mappedBy="member1")
	private List<Follower> followers1;

	//bi-directional many-to-one association to Follower
	@OneToMany(mappedBy="member2")
	private List<Follower> followers2;

	//bi-directional many-to-one association to Item
	@OneToMany(mappedBy="member")
	private List<Item> items;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="city_id")
	private City city;

	//bi-directional many-to-one association to Country
	@ManyToOne
	@JoinColumn(name="country_id")
	private Country country;

	//bi-directional many-to-one association to Gender
	@ManyToOne
	@JoinColumn(name="gender_id")
	private Gender gender;

	//bi-directional many-to-one association to State
	@ManyToOne
	@JoinColumn(name="state_id")
	private State state;

	//bi-directional many-to-one association to UserFavorite
	@OneToMany(mappedBy="member")
	private List<UserFavorite> userFavorites;

	//bi-directional many-to-one association to UserShared
	@OneToMany(mappedBy="member")
	private List<UserShared> userShareds;

	public Member() {
	}

	public Long getMemberId() {
		return this.memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return this.address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getBrandLogo() {
		return this.brandLogo;
	}

	public void setBrandLogo(String brandLogo) {
		this.brandLogo = brandLogo;
	}

	public String getCountryZipcode() {
		return this.countryZipcode;
	}

	public void setCountryZipcode(String countryZipcode) {
		this.countryZipcode = countryZipcode;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public String getDevice() {
		return this.device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsguest() {
		return this.isguest;
	}

	public void setIsguest(Boolean isguest) {
		this.isguest = isguest;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProfilePic() {
		return this.profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getSignupWith() {
		return this.signupWith;
	}

	public void setSignupWith(String signupWith) {
		this.signupWith = signupWith;
	}

	public String getTradeName() {
		return this.tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public List<Basket> getBaskets() {
		return this.baskets;
	}

	public void setBaskets(List<Basket> baskets) {
		this.baskets = baskets;
	}

	public Basket addBasket(Basket basket) {
		getBaskets().add(basket);
		basket.setMember(this);

		return basket;
	}

	public Basket removeBasket(Basket basket) {
		getBaskets().remove(basket);
		basket.setMember(null);

		return basket;
	}

	public List<Basta> getBastas() {
		return this.bastas;
	}

	public void setBastas(List<Basta> bastas) {
		this.bastas = bastas;
	}

	public Basta addBasta(Basta basta) {
		getBastas().add(basta);
		basta.setMember(this);

		return basta;
	}

	public Basta removeBasta(Basta basta) {
		getBastas().remove(basta);
		basta.setMember(null);

		return basta;
	}

	public List<CallDone> getCallDones() {
		return this.callDones;
	}

	public void setCallDones(List<CallDone> callDones) {
		this.callDones = callDones;
	}

	public CallDone addCallDone(CallDone callDone) {
		getCallDones().add(callDone);
		callDone.setMember(this);

		return callDone;
	}

	public CallDone removeCallDone(CallDone callDone) {
		getCallDones().remove(callDone);
		callDone.setMember(null);

		return callDone;
	}

	public List<Follower> getFollowers1() {
		return this.followers1;
	}

	public void setFollowers1(List<Follower> followers1) {
		this.followers1 = followers1;
	}

	public Follower addFollowers1(Follower followers1) {
		getFollowers1().add(followers1);
		followers1.setMember1(this);

		return followers1;
	}

	public Follower removeFollowers1(Follower followers1) {
		getFollowers1().remove(followers1);
		followers1.setMember1(null);

		return followers1;
	}

	public List<Follower> getFollowers2() {
		return this.followers2;
	}

	public void setFollowers2(List<Follower> followers2) {
		this.followers2 = followers2;
	}

	public Follower addFollowers2(Follower followers2) {
		getFollowers2().add(followers2);
		followers2.setMember2(this);

		return followers2;
	}

	public Follower removeFollowers2(Follower followers2) {
		getFollowers2().remove(followers2);
		followers2.setMember2(null);

		return followers2;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setMember(this);

		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setMember(null);

		return item;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Gender getGender() {
		return this.gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<UserFavorite> getUserFavorites() {
		return this.userFavorites;
	}

	public void setUserFavorites(List<UserFavorite> userFavorites) {
		this.userFavorites = userFavorites;
	}

	public UserFavorite addUserFavorite(UserFavorite userFavorite) {
		getUserFavorites().add(userFavorite);
		userFavorite.setMember(this);

		return userFavorite;
	}

	public UserFavorite removeUserFavorite(UserFavorite userFavorite) {
		getUserFavorites().remove(userFavorite);
		userFavorite.setMember(null);

		return userFavorite;
	}

	public List<UserShared> getUserShareds() {
		return this.userShareds;
	}

	public void setUserShareds(List<UserShared> userShareds) {
		this.userShareds = userShareds;
	}

	public UserShared addUserShared(UserShared userShared) {
		getUserShareds().add(userShared);
		userShared.setMember(this);

		return userShared;
	}

	public UserShared removeUserShared(UserShared userShared) {
		getUserShareds().remove(userShared);
		userShared.setMember(null);

		return userShared;
	}

}