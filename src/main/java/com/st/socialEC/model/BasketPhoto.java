package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the basket_photos database table.
 * 
 */
@Entity
@Table(name="basket_photos")
@NamedQuery(name="BasketPhoto.findAll", query="SELECT b FROM BasketPhoto b")
public class BasketPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="basket_photos_id", unique=true, nullable=false)
	private Integer basketPhotosId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="photo_path", length=255)
	private String photoPath;

	@Column(length=255)
	private String thumbnail;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Basket
	@ManyToOne
	@JoinColumn(name="basket_id", nullable=false)
	private Basket basket;

	public BasketPhoto() {
	}

	public Integer getBasketPhotosId() {
		return this.basketPhotosId;
	}

	public void setBasketPhotosId(Integer basketPhotosId) {
		this.basketPhotosId = basketPhotosId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public String getPhotoPath() {
		return this.photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getThumbnail() {
		return this.thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public Basket getBasket() {
		return this.basket;
	}

	public void setBasket(Basket basket) {
		this.basket = basket;
	}

}