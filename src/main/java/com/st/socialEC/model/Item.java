package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the items database table.
 * 
 */
@Entity
@Table(name="items")
@NamedQuery(name="Item.findAll", query="SELECT i FROM Item i")
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="item_id", unique=true, nullable=false)
	private Integer itemId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="available_qty")
	private Integer availableQty;

	@Column(name="available_qty_unit")
	private Integer availableQtyUnit;

	private Integer currecy;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="delivery_fee", nullable=false)
	private double deliveryFee;

	@Column(name="delivery_locations", length=500)
	private String deliveryLocations;

	@Column(nullable=false, length=500)
	private String description;

	@Column(name="free_delivery_locations", length=500)
	private String freeDeliveryLocations;

	@Column(name="harvest_within_nbr_days")
	private Integer harvestWithinNbrDays;

	private Long latitude;

	private Timestamp locationtimestamp;

	private Long longitude;

	@Column(name="min_order_quantity")
	private Integer minOrderQuantity;

	@Column(name="min_order_quantity_unit")
	private Integer minOrderQuantityUnit;

	@Column(nullable=false, length=100)
	private String phone;

	@Column(nullable=false)
	private double prices;

	@Column(nullable=false, length=100)
	private String title;

	@Column(name="trade_name", length=100)
	private String tradeName;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	@Column(name="with_delivery", nullable=false)
	private Boolean withDelivery;

	//bi-directional many-to-one association to CallDone
	@OneToMany(mappedBy="item")
	private List<CallDone> callDones;

	//bi-directional many-to-one association to ItemPhoto
	@OneToMany(mappedBy="item")
	private List<ItemPhoto> itemPhotos;

	//bi-directional many-to-one association to Basket
	@ManyToOne
	@JoinColumn(name="basket_id")
	private Basket basket;

	//bi-directional many-to-one association to Basta
	@ManyToOne
	@JoinColumn(name="basta_id")
	private Basta basta;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category_id", nullable=false)
	private Category category;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member_id", nullable=false)
	private Member member;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne
	@JoinColumn(name="subcategory_id", nullable=false)
	private Subcategory subcategory;

	//bi-directional many-to-one association to Type
	@ManyToOne
	@JoinColumn(name="type_id", nullable=false)
	private Type type;

	//bi-directional many-to-one association to UserFavorite
	@OneToMany(mappedBy="item")
	private List<UserFavorite> userFavorites;

	//bi-directional many-to-one association to UserShared
	@OneToMany(mappedBy="item")
	private List<UserShared> userShareds;

	public Item() {
	}

	public Integer getItemId() {
		return this.itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getAvailableQty() {
		return this.availableQty;
	}

	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	public Integer getAvailableQtyUnit() {
		return this.availableQtyUnit;
	}

	public void setAvailableQtyUnit(Integer availableQtyUnit) {
		this.availableQtyUnit = availableQtyUnit;
	}

	public Integer getCurrecy() {
		return this.currecy;
	}

	public void setCurrecy(Integer currecy) {
		this.currecy = currecy;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public double getDeliveryFee() {
		return this.deliveryFee;
	}

	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public String getDeliveryLocations() {
		return this.deliveryLocations;
	}

	public void setDeliveryLocations(String deliveryLocations) {
		this.deliveryLocations = deliveryLocations;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFreeDeliveryLocations() {
		return this.freeDeliveryLocations;
	}

	public void setFreeDeliveryLocations(String freeDeliveryLocations) {
		this.freeDeliveryLocations = freeDeliveryLocations;
	}

	public Integer getHarvestWithinNbrDays() {
		return this.harvestWithinNbrDays;
	}

	public void setHarvestWithinNbrDays(Integer harvestWithinNbrDays) {
		this.harvestWithinNbrDays = harvestWithinNbrDays;
	}

	public Long getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Long latitude) {
		this.latitude = latitude;
	}

	public Timestamp getLocationtimestamp() {
		return this.locationtimestamp;
	}

	public void setLocationtimestamp(Timestamp locationtimestamp) {
		this.locationtimestamp = locationtimestamp;
	}

	public Long getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Long longitude) {
		this.longitude = longitude;
	}

	public Integer getMinOrderQuantity() {
		return this.minOrderQuantity;
	}

	public void setMinOrderQuantity(Integer minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}

	public Integer getMinOrderQuantityUnit() {
		return this.minOrderQuantityUnit;
	}

	public void setMinOrderQuantityUnit(Integer minOrderQuantityUnit) {
		this.minOrderQuantityUnit = minOrderQuantityUnit;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getPrices() {
		return this.prices;
	}

	public void setPrices(double prices) {
		this.prices = prices;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTradeName() {
		return this.tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public Boolean getWithDelivery() {
		return this.withDelivery;
	}

	public void setWithDelivery(Boolean withDelivery) {
		this.withDelivery = withDelivery;
	}

	public List<CallDone> getCallDones() {
		return this.callDones;
	}

	public void setCallDones(List<CallDone> callDones) {
		this.callDones = callDones;
	}

	public CallDone addCallDone(CallDone callDone) {
		getCallDones().add(callDone);
		callDone.setItem(this);

		return callDone;
	}

	public CallDone removeCallDone(CallDone callDone) {
		getCallDones().remove(callDone);
		callDone.setItem(null);

		return callDone;
	}

	public List<ItemPhoto> getItemPhotos() {
		return this.itemPhotos;
	}

	public void setItemPhotos(List<ItemPhoto> itemPhotos) {
		this.itemPhotos = itemPhotos;
	}

	public ItemPhoto addItemPhoto(ItemPhoto itemPhoto) {
		getItemPhotos().add(itemPhoto);
		itemPhoto.setItem(this);

		return itemPhoto;
	}

	public ItemPhoto removeItemPhoto(ItemPhoto itemPhoto) {
		getItemPhotos().remove(itemPhoto);
		itemPhoto.setItem(null);

		return itemPhoto;
	}

	public Basket getBasket() {
		return this.basket;
	}

	public void setBasket(Basket basket) {
		this.basket = basket;
	}

	public Basta getBasta() {
		return this.basta;
	}

	public void setBasta(Basta basta) {
		this.basta = basta;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Subcategory getSubcategory() {
		return this.subcategory;
	}

	public void setSubcategory(Subcategory subcategory) {
		this.subcategory = subcategory;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<UserFavorite> getUserFavorites() {
		return this.userFavorites;
	}

	public void setUserFavorites(List<UserFavorite> userFavorites) {
		this.userFavorites = userFavorites;
	}

	public UserFavorite addUserFavorite(UserFavorite userFavorite) {
		getUserFavorites().add(userFavorite);
		userFavorite.setItem(this);

		return userFavorite;
	}

	public UserFavorite removeUserFavorite(UserFavorite userFavorite) {
		getUserFavorites().remove(userFavorite);
		userFavorite.setItem(null);

		return userFavorite;
	}

	public List<UserShared> getUserShareds() {
		return this.userShareds;
	}

	public void setUserShareds(List<UserShared> userShareds) {
		this.userShareds = userShareds;
	}

	public UserShared addUserShared(UserShared userShared) {
		getUserShareds().add(userShared);
		userShared.setItem(this);

		return userShared;
	}

	public UserShared removeUserShared(UserShared userShared) {
		getUserShareds().remove(userShared);
		userShared.setItem(null);

		return userShared;
	}

}