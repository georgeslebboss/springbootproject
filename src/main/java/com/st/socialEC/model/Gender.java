package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the gender database table.
 * 
 */
@Entity
@Table(name="gender")
@NamedQuery(name="Gender.findAll", query="SELECT g FROM Gender g")
public class Gender implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="gender_id", unique=true, nullable=false)
	private Integer genderId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="gender_desc", nullable=false, length=50)
	private String genderDesc;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Member
	@OneToMany(mappedBy="gender")
	private List<Member> members;

	public Gender() {
	}

	public Integer getGenderId() {
		return this.genderId;
	}

	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public String getGenderDesc() {
		return this.genderDesc;
	}

	public void setGenderDesc(String genderDesc) {
		this.genderDesc = genderDesc;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public List<Member> getMembers() {
		return this.members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Member addMember(Member member) {
		getMembers().add(member);
		member.setGender(this);

		return member;
	}

	public Member removeMember(Member member) {
		getMembers().remove(member);
		member.setGender(null);

		return member;
	}

}