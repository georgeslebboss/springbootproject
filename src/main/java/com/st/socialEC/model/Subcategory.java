package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the subcategory database table.
 * 
 */
@Entity
@Table(name="subcategory")
@NamedQuery(name="Subcategory.findAll", query="SELECT s FROM Subcategory s")
public class Subcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="subcategory_id", unique=true, nullable=false)
	private Integer subcategoryId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="subcategory_name", nullable=false, length=255)
	private String subcategoryName;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Basket
	@OneToMany(mappedBy="subcategory")
	private List<Basket> baskets;

	//bi-directional many-to-one association to Basta
	@OneToMany(mappedBy="subcategory")
	private List<Basta> bastas;

	//bi-directional many-to-one association to Item
	@OneToMany(mappedBy="subcategory")
	private List<Item> items;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category_id", nullable=false)
	private Category category;

	public Subcategory() {
	}

	public Integer getSubcategoryId() {
		return this.subcategoryId;
	}

	public void setSubcategoryId(Integer subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public String getSubcategoryName() {
		return this.subcategoryName;
	}

	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public List<Basket> getBaskets() {
		return this.baskets;
	}

	public void setBaskets(List<Basket> baskets) {
		this.baskets = baskets;
	}

	public Basket addBasket(Basket basket) {
		getBaskets().add(basket);
		basket.setSubcategory(this);

		return basket;
	}

	public Basket removeBasket(Basket basket) {
		getBaskets().remove(basket);
		basket.setSubcategory(null);

		return basket;
	}

	public List<Basta> getBastas() {
		return this.bastas;
	}

	public void setBastas(List<Basta> bastas) {
		this.bastas = bastas;
	}

	public Basta addBasta(Basta basta) {
		getBastas().add(basta);
		basta.setSubcategory(this);

		return basta;
	}

	public Basta removeBasta(Basta basta) {
		getBastas().remove(basta);
		basta.setSubcategory(null);

		return basta;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setSubcategory(this);

		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setSubcategory(null);

		return item;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}