package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the type database table.
 * 
 */
@Entity
@Table(name="type")
@NamedQuery(name="Type.findAll", query="SELECT t FROM Type t")
public class Type implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="type_id", unique=true, nullable=false)
	private Integer typeId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="type_name", nullable=false, length=255)
	private String typeName;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Basket
	@OneToMany(mappedBy="type")
	private List<Basket> baskets;

	//bi-directional many-to-one association to Basta
	@OneToMany(mappedBy="type")
	private List<Basta> bastas;

	//bi-directional many-to-one association to Item
	@OneToMany(mappedBy="type")
	private List<Item> items;

	public Type() {
	}

	public Integer getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public List<Basket> getBaskets() {
		return this.baskets;
	}

	public void setBaskets(List<Basket> baskets) {
		this.baskets = baskets;
	}

	public Basket addBasket(Basket basket) {
		getBaskets().add(basket);
		basket.setType(this);

		return basket;
	}

	public Basket removeBasket(Basket basket) {
		getBaskets().remove(basket);
		basket.setType(null);

		return basket;
	}

	public List<Basta> getBastas() {
		return this.bastas;
	}

	public void setBastas(List<Basta> bastas) {
		this.bastas = bastas;
	}

	public Basta addBasta(Basta basta) {
		getBastas().add(basta);
		basta.setType(this);

		return basta;
	}

	public Basta removeBasta(Basta basta) {
		getBastas().remove(basta);
		basta.setType(null);

		return basta;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setType(this);

		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setType(null);

		return item;
	}

}