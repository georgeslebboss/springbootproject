package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the followers database table.
 * 
 */
@Embeddable
public class FollowerPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="member_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer memberId;

	@Column(name="follower_id", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer followerId;

	public FollowerPK() {
	}
	public Integer getMemberId() {
		return this.memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public Integer getFollowerId() {
		return this.followerId;
	}
	public void setFollowerId(Integer followerId) {
		this.followerId = followerId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FollowerPK)) {
			return false;
		}
		FollowerPK castOther = (FollowerPK)other;
		return 
			this.memberId.equals(castOther.memberId)
			&& this.followerId.equals(castOther.followerId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.memberId.hashCode();
		hash = hash * prime + this.followerId.hashCode();
		
		return hash;
	}
}