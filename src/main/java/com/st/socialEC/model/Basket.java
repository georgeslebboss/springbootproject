package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the basket database table.
 * 
 */
@Entity
@Table(name="basket")
@NamedQuery(name="Basket.findAll", query="SELECT b FROM Basket b")
public class Basket implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="basket_id", unique=true, nullable=false)
	private Integer basketId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="delivery_fee")
	private double deliveryFee;

	@Column(name="delivery_locations", length=500)
	private String deliveryLocations;

	@Column(nullable=false, length=500)
	private String description;

	@Column(name="free_delivery_locations", length=500)
	private String freeDeliveryLocations;

	private Long latitude;

	private Timestamp locationtimestamp;

	private Long longitude;

	@Column(nullable=false, length=100)
	private String phone;

	@Column(nullable=false, length=100)
	private String title;

	@Column(name="trade_name", length=100)
	private String tradeName;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	@Column(name="with_delivery")
	private Boolean withDelivery;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="category_id", nullable=false)
	private Category category;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member_id", nullable=false)
	private Member member;

	//bi-directional many-to-one association to Subcategory
	@ManyToOne
	@JoinColumn(name="subcategory_id", nullable=false)
	private Subcategory subcategory;

	//bi-directional many-to-one association to Type
	@ManyToOne
	@JoinColumn(name="type_id", nullable=false)
	private Type type;

	//bi-directional many-to-one association to BasketPhoto
	@OneToMany(mappedBy="basket")
	private List<BasketPhoto> basketPhotos;

	//bi-directional many-to-one association to Item
	@OneToMany(mappedBy="basket")
	private List<Item> items;

	public Basket() {
	}

	public Integer getBasketId() {
		return this.basketId;
	}

	public void setBasketId(Integer basketId) {
		this.basketId = basketId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public double getDeliveryFee() {
		return this.deliveryFee;
	}

	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public String getDeliveryLocations() {
		return this.deliveryLocations;
	}

	public void setDeliveryLocations(String deliveryLocations) {
		this.deliveryLocations = deliveryLocations;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFreeDeliveryLocations() {
		return this.freeDeliveryLocations;
	}

	public void setFreeDeliveryLocations(String freeDeliveryLocations) {
		this.freeDeliveryLocations = freeDeliveryLocations;
	}

	public Long getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Long latitude) {
		this.latitude = latitude;
	}

	public Timestamp getLocationtimestamp() {
		return this.locationtimestamp;
	}

	public void setLocationtimestamp(Timestamp locationtimestamp) {
		this.locationtimestamp = locationtimestamp;
	}

	public Long getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Long longitude) {
		this.longitude = longitude;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTradeName() {
		return this.tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public Boolean getWithDelivery() {
		return this.withDelivery;
	}

	public void setWithDelivery(Boolean withDelivery) {
		this.withDelivery = withDelivery;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Subcategory getSubcategory() {
		return this.subcategory;
	}

	public void setSubcategory(Subcategory subcategory) {
		this.subcategory = subcategory;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<BasketPhoto> getBasketPhotos() {
		return this.basketPhotos;
	}

	public void setBasketPhotos(List<BasketPhoto> basketPhotos) {
		this.basketPhotos = basketPhotos;
	}

	public BasketPhoto addBasketPhoto(BasketPhoto basketPhoto) {
		getBasketPhotos().add(basketPhoto);
		basketPhoto.setBasket(this);

		return basketPhoto;
	}

	public BasketPhoto removeBasketPhoto(BasketPhoto basketPhoto) {
		getBasketPhotos().remove(basketPhoto);
		basketPhoto.setBasket(null);

		return basketPhoto;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item addItem(Item item) {
		getItems().add(item);
		item.setBasket(this);

		return item;
	}

	public Item removeItem(Item item) {
		getItems().remove(item);
		item.setBasket(null);

		return item;
	}

}