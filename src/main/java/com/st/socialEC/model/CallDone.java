package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the call_done database table.
 * 
 */
@Entity
@Table(name="call_done")
@NamedQuery(name="CallDone.findAll", query="SELECT c FROM CallDone c")
public class CallDone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="call_done_id", unique=true, nullable=false)
	private Long callDoneId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="call_from", nullable=false)
	private Integer callFrom;

	@Column(name="call_to", nullable=false)
	private Integer callTo;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Item
	@ManyToOne
	@JoinColumn(name="item_id", nullable=false)
	private Item item;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member_id", nullable=false)
	private Member member;

	public CallDone() {
	}

	public Long getCallDoneId() {
		return this.callDoneId;
	}

	public void setCallDoneId(Long callDoneId) {
		this.callDoneId = callDoneId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getCallFrom() {
		return this.callFrom;
	}

	public void setCallFrom(Integer callFrom) {
		this.callFrom = callFrom;
	}

	public Integer getCallTo() {
		return this.callTo;
	}

	public void setCallTo(Integer callTo) {
		this.callTo = callTo;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

}