package com.st.socialEC.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the user_favorites database table.
 * 
 */
@Entity
@Table(name="user_favorites")
@NamedQuery(name="UserFavorite.findAll", query="SELECT u FROM UserFavorite u")
public class UserFavorite implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserFavoritePK id;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="date_create")
	private Timestamp dateCreate;

	@Column(name="date_modified")
	private Timestamp dateModified;

	@Column(name="user_created")
	private Integer userCreated;

	@Column(name="user_modified")
	private Integer userModified;

	//bi-directional many-to-one association to Item
	@ManyToOne
	@JoinColumn(name="item_id", nullable=false, insertable=false, updatable=false)
	private Item item;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member_user_id", nullable=false, insertable=false, updatable=false)
	private Member member;

	public UserFavorite() {
	}

	public UserFavoritePK getId() {
		return this.id;
	}

	public void setId(UserFavoritePK id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Timestamp getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	public Integer getUserCreated() {
		return this.userCreated;
	}

	public void setUserCreated(Integer userCreated) {
		this.userCreated = userCreated;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

}