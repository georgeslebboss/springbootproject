package com.st.socialEC.model;

 
import javax.persistence.*;



import org.springframework.data.geo.Point;

import com.fasterxml.jackson.annotation.JsonIgnore;


import java.util.Date;
import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@Table(name="city")

public class City   {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "seq_city")
	@Column(name="city_id", unique=true, nullable=false)
	private Long cityId;

	@Column(nullable=false)
	private Boolean active;

	@Column(name="city_name", nullable=false, length=255)
	private String cityName;

	//@Column(columnDefinition = "Geometry", nullable = true) 
	//@Type(type = "org.hibernate.spatial.GeometryType")
	private Point coordinate;

	@Column(name="date_create")
	private Date dateCreate;

	@Column(name="date_modified")
	private Date dateModified;

	@Column(name="user_create")
	private Integer userCreate;

	@Column(name="user_modified")
	private Integer userModified;



	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="country_id", nullable=false)
	@JsonIgnore
	private Country country;
	

	//bi-directional many-to-one association to State
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="state_id", nullable=false)
	@JsonIgnore
	private State state;

	//bi-directional many-to-one association to Member
	@OneToMany(mappedBy="city")
	@JsonIgnore
 	private List<Member> members;
	
	
	public City() {
	}

	public Long getCityId() {
		return this.cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	
	public Point getCoordinate() {
		return this.coordinate;
	}

	public void setCoordinate(Point coordinate) {
		this.coordinate = coordinate;
	}

	public Date getDateCreate() {
		return this.dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Integer getUserCreate() {
		return this.userCreate;
	}

	public void setUserCreate(Integer userCreate) {
		this.userCreate = userCreate;
	}

	public Integer getUserModified() {
		return this.userModified;
	}

	public void setUserModified(Integer userModified) {
		this.userModified = userModified;
	}


	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<Member> getMembers() {
		return this.members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Member addMember(Member member) {
		getMembers().add(member);
		member.setCity(this);

		return member;
	}

	public Member removeMember(Member member) {
		getMembers().remove(member);
		member.setCity(null);

		return member;
	}

}
