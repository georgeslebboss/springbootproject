package com.st.socialEC.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
