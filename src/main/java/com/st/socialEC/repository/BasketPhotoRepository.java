package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.BasketPhoto;

@Repository
public interface  BasketPhotoRepository extends JpaRepository<BasketPhoto, Long>{

	 
}
