package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.Basket;

@Repository
public interface  BasketRepository extends JpaRepository<Basket, Long>{

	 
}
