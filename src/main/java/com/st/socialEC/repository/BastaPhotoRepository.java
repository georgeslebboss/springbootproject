package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.BastaPhoto;

@Repository
public interface  BastaPhotoRepository extends JpaRepository<BastaPhoto, Long>{

	 
}
