package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.Follower;
 

@Repository
public interface FollowerRepository extends JpaRepository<Follower, Long>{

}
