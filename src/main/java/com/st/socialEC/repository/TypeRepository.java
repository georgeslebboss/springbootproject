package com.st.socialEC.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.Type;

@Repository
public interface TypeRepository extends JpaRepository <Type,Long>{

}
