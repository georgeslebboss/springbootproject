package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

}
