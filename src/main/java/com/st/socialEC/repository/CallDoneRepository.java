package com.st.socialEC.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.socialEC.model.CallDone;

@Repository
public interface  CallDoneRepository extends JpaRepository<CallDone, Long>{

	 
}
