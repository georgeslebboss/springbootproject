package com.st.socialEC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.st.socialEC.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class SocialECApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialECApplication.class, args);
	}
}
